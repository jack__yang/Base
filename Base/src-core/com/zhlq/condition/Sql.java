package com.zhlq.condition;


public class Sql {
	
	// constant
	public final static String TRUE = "1=1";
	public final static String FALSE = "1=0";
	public final static String SPACE = " ";

	// insert
	public final static String INSERT_INTO = "INSERT INTO";
	public final static String VALUES = "VALUES";

	// select
	public final static String SELECT = "SELECT";
	public final static String INTO = "INTO";
	public final static String FROM = "FROM";

	// update
	public final static String UPDATE = "UPDATE";
	public final static String SET = "SET";

	// delete
	public final static String DELETE_FROM = "DELETE FROM";
	
	// assist
	public final static String DISTINCT = "DISTINCT";
	public final static String AS = "AS";
	
	// join
	public final static String JOIN = "JOIN";
	public final static String INNER_JOIN = "INNER JOIN";
	public final static String LEFT_JOIN = "LEFT JOIN";
	public final static String LEFT_OUTER_JOIN = "LEFT OUTER JOIN";
	public final static String RIGHT_JOIN = "RIGHT JOIN";
	public final static String RIGHT_OUTER_JOIN = "RIGHT OUTER JOIN";
	public final static String FULL_JOIN = "FULL JOIN";
	public final static String FULL_OUTER_JOIN = "FULL OUTER JOIN";
	public final static String ON = "ON";
	
	// union
	public final static String UNION = "UNION";
	public final static String UNION_ALL = "UNION ALL";

	// where
	public final static String WHERE = "WHERE";
	public final static String WHERE_TRUE = "WHERE" + SPACE + TRUE;
	public final static String WHERE_FALSE = "WHERE" + SPACE + FALSE;
	public final static String AND = "AND";
	public final static String OR = "OR";
	public final static String NOT = "NOT";
	public final static String LIKE = "LIKE";
	public final static String IN = "IN";
	public final static String BETWEEN = "BETWEEN";

	// order
	public final static String ORDER_BY = "ORDER BY";
	public final static String DESC = "DESC";
	public final static String ASC = "ASC";

	// symbol
	public final static String EQUAL = "=";
	public final static String All = "*";
	public final static String COMMA = ",";
	public final static String PERCENT = "%";
	public final static String UNDERLINE = "_";
	public final static String XOR = "^";
	public final static String EXCLAMATION = "!";
	public final static String POUND = "#";
	public final static String SINGLE_QUOTE = "'";
	public final static String DOUBLE_QUOTE = "\"";
	public final static String PARENTHESES = "()";
	public final static String PARENTHESES_LEFT = "(";
	public final static String PARENTHESES_RIGHT = ")";
	public final static String BRACKET = "[]";
	public final static String BRACKET_LEFT = "[";
	public final static String BRACKET_RIGHT = "]";
	public final static String SEMICOLON = ";";
	public final static String QUESTION = "?";
	
	// create
	public final static String CREATE_DATABASE = "CREATE DATABASE";
	public final static String CREATE_TABLE = "CREATE TABLE";
	
	// CONSTRAINT

	// format
	public final static String TAB = "\t";
	public final static String TAB_SPACE = "    ";
	public final static String NEWLINE = "\n";

	// custom
//	private List<String> cols = new LinkedList<String>();
//	private String table = "";

}

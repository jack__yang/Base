package com.zhlq.exception;

public class ServiceConstant {

	public static final String NULL_VALUE = "空值";
	public static final String ARRAY_LENGTH_ZERO = "数组长度为0";
	public static final String BEAN_IS_BASE = "JavaBean对象是基础数据类型";
	public static final String UNKNOW = "未知异常";

}

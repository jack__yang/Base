package com.zhlq.exception;

public class WhereException extends BaseException {

	/**
	 * @Fields serialVersionUID : 序列化ID
	 */
	private static final long serialVersionUID = 1L;

	public WhereException() {
		super();
	}

	public WhereException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public WhereException(String tips, Throwable cause) {
		super(tips, cause);
	}

	public WhereException(String tips) {
		super(tips);
	}

	public WhereException(Throwable cause) {
		super(cause);
	}
	
}

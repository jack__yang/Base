package com.zhlq.exception;

public class ServiceException extends BaseException {

	/**
	 * @Fields serialVersionUID : 序列化ID
	 */
	private static final long serialVersionUID = 1L;

	public ServiceException() {
		super();
	}

	public ServiceException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ServiceException(String tips, Throwable cause) {
		super(tips, cause);
	}

	public ServiceException(String tips) {
		super(tips);
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}

}

package com.zhlq.exception;

/**
 * @ClassName DaoConstant
 * @Description Dao常量
 * @author ZHLQ
 * @date 2014年9月11日 下午10:00:11
 */
public class DaoConstant {

	/**
	 * @Fields TABLE_IS_EMPTY : 表名为空
	 */
	public static final String TABLE_IS_EMPTY = "表名为空！";
	/**
	 * @Fields CONN_IS_NULL : 链接为空
	 */
	public static final String CONN_IS_NULL = "链接为空！";
	/**
	 * @Fields PSTMT_IS_NULL : 预编译语句为空
	 */
	public static final String PSTMT_IS_NULL = "预编译语句为空！";
	/**
	 * @Fields RS_IS_NULL : 结果集为空
	 */
	public static final String RS_IS_NULL = "结果集为空！";
	/**
	 * @Fields RSMD_IS_NULL : 结果集元数据为空
	 */
	public static final String RSMD_IS_NULL = "结果集元数据为空！";
	/**
	 * @Fields UNKNOW_EXCEPTION : 未知异常
	 */
	public static final String UNKNOW_EXCEPTION = "未知异常！";

}

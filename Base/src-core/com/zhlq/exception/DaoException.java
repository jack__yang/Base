package com.zhlq.exception;

/**
 * @ClassName DaoException
 * @Description dao异常
 * @author ZHLQ
 * @date 2014年9月11日 下午9:56:13
 */
public class DaoException extends BaseException {

	/**
	 * @Fields serialVersionUID : 序列化编号
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @Title DaoException
	 * @Description 创建一个新的实例 DaoException。
	 */
	public DaoException() {
		super();
	}

	/**
	 * @Title DaoException
	 * @Description 创建一个新的实例 DaoException。
	 * @param messageJDK提示信息
	 * @param cause异常原因
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DaoException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @Title DaoException
	 * @Description 创建一个新的实例 DaoException。
	 * @param tips提示信息
	 * @param cause异常原因
	 */
	public DaoException(String tips, Throwable cause) {
		super(tips, cause);
	}

	/**
	 * @Title DaoException
	 * @Description 创建一个新的实例 DaoException。
	 * @param tips提示信息
	 */
	public DaoException(String tips) {
		super(tips);
	}

	/**
	 * @Title DaoException
	 * @Description 创建一个新的实例 DaoException。
	 * @param cause异常原因
	 */
	public DaoException(Throwable cause) {
		super(cause);
	}

}

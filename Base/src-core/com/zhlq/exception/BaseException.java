package com.zhlq.exception;

/**
 * @ClassName BaseException
 * @Description 基础异常
 * @author ZHLQ
 * @date 2014年9月11日 下午9:54:19
 */
public class BaseException extends RuntimeException {

	/**
	 * @Fields serialVersionUID : 序列化编号
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * @Fields tips : 提示信息
	 */
	private String tips = "";

	/**
	 * @Title BaseException
	 * @Description 创建一个新的实例 BaseException。
	 */
	public BaseException() {
		super();
	}

	/**
	 * @Title BaseException
	 * @Description 创建一个新的实例 BaseException。
	 * @param tips提示信息
	 */
	public BaseException(String tips) {
		this.tips = tips;
	}

	/**
	 * @Title BaseException
	 * @Description 创建一个新的实例 BaseException。
	 * @param cause异常原因
	 */
	public BaseException(Throwable cause) {
		super(cause);
	}

	/**
	 * @Title BaseException
	 * @Description 创建一个新的实例 BaseException。
	 * @param messageJDK提示信息
	 * @param cause异常原因
	 */
	public BaseException(String tips, Throwable cause) {
		super(cause);
		this.tips = tips;
	}

	/**
	 * @Title BaseException
	 * @Description 创建一个新的实例 BaseException。
	 * @param messageJDK提示信息
	 * @param cause异常原因
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public BaseException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @Title getTips
	 * @Description 获取提示信息
	 * @return String 返回类型
	 */
	public String getTips() {
		return tips;
	}

	/**
	 * @Title setTips
	 * @Description 设置提示信息
	 * @param tips提示信息
	 * @return void 返回类型
	 */
	public void setTips(String tips) {
		this.tips = tips;
	}

}

package com.zhlq.page;

import java.util.List;

/**
 * @ClassName PageResult
 * @Description 分页查询的结果JavaBean
 * @author ZHLQ
 * @date 2014年9月6日 上午11:38:24
 */
public class PageResult {

	/**
	 * @Fields datas : 数据
	 */
	private List<?> datas;
	/**
	 * @Fields page : 分页
	 */
	private Page page;

	/**
	 * @Title PageResult
	 * @Description 创建一个新的实例 PageResult。
	 */
	public PageResult() {
		super();
	}

	/**
	 * @Title getDatas
	 * @Description 获取数据
	 * @return List<?> 返回类型
	 */
	public List<?> getDatas() {
		return datas;
	}

	/**
	 * @Title setDatas
	 * @Description 设置数据
	 * @param datas数据
	 * @return void 返回类型
	 */
	public void setDatas(List<?> datas) {
		this.datas = datas;
	}

	/**
	 * @Title getPage
	 * @Description 获取分页
	 * @return Page 返回类型
	 */
	public Page getPage() {
		return page;
	}

	/**
	 * @Title setPage
	 * @Description 设置分页
	 * @param page分页
	 * @return void 返回类型
	 */
	public void setPage(Page page) {
		this.page = page;
	}

}

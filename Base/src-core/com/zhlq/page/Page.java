package com.zhlq.page;

/**
 * @ClassName Page
 * @Description 分页JavaBean
 * @author ZHLQ
 * @date 2014年9月6日 上午10:54:21
 */
public class Page {

	/**
	 * @Fields hasLast : 是否有上一页
	 */
	private boolean hasLast = false;
	/**
	 * @Fields hasNext : 是否有下一页
	 */
	private boolean hasNext = false;
	/**
	 * @Fields current : 当前页数
	 */
	private long current = 1;
	/**
	 * @Fields index : 数据库页码索引
	 */
	private long index = 0;
	/**
	 * @Fields maxPage : 最大页数
	 */
	private long maxPage = 0;
	/**
	 * @Fields size : 每页大小
	 */
	private long size = 20;
	/**
	 * @Fields count : 总记录数
	 */
	private long count = 0;
	/**
	 * @Fields maxShow : 页码最大显示数
	 */
	private long maxShow = 10;
	/**
	 * @Fields showBegin : 开始显示页码
	 */
	private long showBegin = 0;
	/**
	 * @Fields showEnd : 结束显示页码
	 */
	private long showEnd = 0;

	/**
	 * @Title Page
	 * @Description 创建一个新的实例 Page。
	 * @注意事项：需要配合set方法使用
	 */
	public Page() {
	}

	/**
	 * @Title Page
	 * @Description 创建一个新的实例 Page。
	 * @适用条件：已获取当前页码，默认每页大小
	 * @param current当前页码
	 */
	public Page(long current) {
		setCurrent(current);
	}

	/**
	 * @Title Page
	 * @Description 创建一个新的实例 Page。
	 * @适用条件：已获取当前页码，每页大小
	 * @执行流程：设置当前页码->设置每页大小
	 * @param current当前页码
	 * @param size每页大小
	 */
	public Page(long current, long size) {
		setCurrent(current);
		setSize(size);
	}

	/**
	 * @Title getCurrent
	 * @Description 获取当前页码
	 * @return long 返回类型
	 */
	public long getCurrent() {
		return current;
	}

	/**
	 * @Title setCurrent
	 * @Description 设置当前页码
	 * @param current
	 * @return void 返回类型
	 */
	public void setCurrent(long current) {
		if (current >= 0) {
			// 非负数：赋值
			this.current = current;
		} else {
			// 负数：默认为0
			this.current = 0;
		}
	}

	/**
	 * @Title getShowBegin
	 * @Description 获取开始显示页码
	 * @return long 返回类型
	 */
	public long getShowBegin() {
		getMaxPage();// 设置最大页码
		getMaxShow();// 设置最大显示页
		getIndex();// 数据库页码索引
		// 显示页码居中
		if (maxPage >= maxShow) {
			// 最大页码>=最大显示页码
			showBegin = index - maxShow / 2;
			showEnd = index + (maxShow - maxShow / 2);
		} else {
			// 最大页码<最大显示页码
			showBegin = (index - maxShow / 2 > 1) ? index - maxShow / 2 : 1;
			showEnd = (index + (maxShow - maxShow / 2)) < maxPage ? index
					+ (maxShow - maxShow / 2) : maxPage;
		}
		return showBegin;
	}

	/**
	 * @Title setShowBegin
	 * @Description 设置开始显示页码
	 * @param showBegin
	 * @return void 返回类型
	 */
	public void setShowBegin(long showBegin) {
		if (showBegin > 1) {
			// 正整数：赋值
			this.showBegin = showBegin;
		} else {
			// 非正整数：默认为1
			this.showBegin = 1;
		}
	}

	/**
	 * @Title getShowEnd
	 * @Description 获取结束显示页码
	 * @return long 返回类型
	 */
	public long getShowEnd() {
		getMaxPage(); // 获取最大页码
		getMaxShow(); // 获取最大显示页码
		getIndex(); // 获取数据库页码索引
		// 页码居中
		if (maxPage >= maxShow) {
			// 最大页码>=最大显示页码
			showBegin = index - maxShow / 2;
			showEnd = index + (maxShow - maxShow / 2);
		} else {
			// 最大页码<最大显示页码
			showBegin = (index - maxShow / 2 > 1) ? index - maxShow / 2 : 1;
			showEnd = (index + (maxShow - maxShow / 2)) < maxPage ? index
					+ (maxShow - maxShow / 2) : maxPage;
		}
		return showEnd;
	}

	/**
	 * @Title setShowEnd
	 * @Description 设置结束显示页码
	 * @param showEnd最优显示页码
	 * @return void 返回类型
	 */
	public void setShowEnd(long showEnd) {
		if (showEnd > 1) {
			// 正整数：赋值
			this.showEnd = showEnd;
		} else {
			// 非正整数：默认为1
			this.showEnd = 1;
		}
	}

	/**
	 * @Title getMaxShow
	 * @Description 获取最大显示页码
	 * @return long 返回类型
	 */
	public long getMaxShow() {
		return maxShow;
	}

	/**
	 * @Title setMaxShow
	 * @Description 设置最大显示页码
	 * @param maxShow最大显示页码
	 * @return void 返回类型
	 */
	public void setMaxShow(long maxShow) {
		if (maxShow > 0) {
			// 正整数：赋值
			this.maxShow = maxShow;
		} else {
			// 非正整数：默认为0
			this.maxPage = 0;
		}
	}

	/**
	 * @Title getHasLast
	 * @Description 获取是否有上一页
	 * @return boolean 返回类型
	 */
	public boolean getHasLast() {
		getCurrent();// 获取当前页码
		if (current > 1) {
			// 页码是1
			hasLast = true;
		} else {
			// 页码不是1
			hasLast = false;
		}
		return hasLast;
	}

	/**
	 * @Title setHasLast
	 * @Description 设置是有有上一页
	 * @param hasLast是否有上一页
	 * @return void 返回类型
	 */
	public void setHasLast(boolean hasLast) {
		this.hasLast = hasLast;
	}

	/**
	 * @Title getHasNext
	 * @Description 获取是否有下一页
	 * @return boolean 返回类型
	 */
	public boolean getHasNext() {
		getCurrent();// 获取当前页码
		getMaxPage();// 获取最大页码
		if (current < maxPage) {
			// 当前页码<最大页码
			hasNext = true;
		} else {
			// 当前页码>最大页码
			hasNext = false;
		}
		return hasNext;
	}

	/**
	 * @Title setHasNext
	 * @Description 设置是否有下一页
	 * @param hasNext是否有下一页
	 * @return void 返回类型
	 */
	public void setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
	}

	/**
	 * @Title getIndex
	 * @Description 获取数据库页码索引
	 * @return long 返回类型
	 */
	public long getIndex() {
		setIndex(getCurrent() - 1);// 设置数据库页码索引
		return index;
	}

	/**
	 * @Title setIndex
	 * @Description 设置数据库页码索引
	 * @param index数据库页码索引
	 * @return void 返回类型
	 */
	public void setIndex(long index) {
		if (index > 0) {
			// 正整数：赋值
			this.index = index;
		} else {
			// 非正整数：默认为0
			this.index = 0;
		}
	}

	/**
	 * @Title getSize
	 * @Description 获取每页大小
	 * @return long 返回类型
	 */
	public long getSize() {
		return size;
	}

	/**
	 * @Title setSize
	 * @Description 设置每页大小
	 * @return void 返回类型
	 */
	public void setSize(long size) {
		if (size > 1) {
			// 大于1：赋值
			this.size = size;
		} else {
			// 小于等于1：默认为1
			this.size = 1;
		}
	}

	/**
	 * @Title getCount
	 * @Description 获取总记录数
	 * @return long 返回类型
	 */
	public long getCount() {
		return count;
	}

	/**
	 * @Title setCount
	 * @Description 设置总记录数
	 * @return void 返回类型
	 */
	public void setCount(long count) {
		if (count > 0) {
			// 正整数：赋值
			this.count = count;
		} else {
			// 非正整数：默认为0
			this.count = 0;
		}
	}

	/**
	 * @Title getMaxPage
	 * @Description 获取最大页码
	 * @return long 返回类型
	 */
	public long getMaxPage() {
		getCount();// 获取总记录数
		getSize();// 获取每页大小
		maxPage = (count % size == 0) ? count / size : count / size + 1;
		return maxPage;
	}

	/**
	 * @Title setMaxPage
	 * @Description 设置最大页码
	 * @param maxPage
	 *            最大页码
	 * @return void 返回类型
	 */
	public void setMaxPage(long maxPage) {
		if (maxPage > 0) {
			// 正整数：赋值
			this.maxPage = maxPage;
		} else {
			// 非正整数：默认为0
			this.maxPage = 0;
		}
	}

	/**
	 * @Title toString
	 * @Description 对象转换为字符串
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		getHasLast();
		getHasNext();
		getCurrent();
		getIndex();
		getMaxPage();
		getSize();
		getCount();
		getMaxShow();
		getShowBegin();
		getShowEnd();
		return "Page [hasLast=" + hasLast + ", hasNext=" + hasNext
				+ ", current=" + current + ", index=" + index + ", maxPage="
				+ maxPage + ", size=" + size + ", count=" + count
				+ ", maxShow=" + maxShow + ", showBegin=" + showBegin
				+ ", showEnd=" + showEnd + "]";
	}

}

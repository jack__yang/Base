package com.zhlq.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.zhlq.condition.Where;
import com.zhlq.core.dao.BaseDao;
import com.zhlq.core.service.BaseService;
import com.zhlq.page.Page;
import com.zhlq.page.PageResult;

/**
 * @ClassName BaseServiceImpl
 * @Description 基础ServiceImpl
 * @author ZHLQ
 * @date 2014年9月10日 下午11:35:20
 */
public class BaseServiceImpl implements BaseService {
	
	/**
	 * @Fields baseDao : 基础Dao
	 */
	private BaseDao baseDao;
	
	/**
	 * @Fields clazzs : 基本数据类型和他们的封装类类型
	 */
	private static List<Class<?>> clazzs;
	
	static {
		clazzs = new ArrayList<Class<?>>();
		clazzs.add(byte.class);
		clazzs.add(short.class);
		clazzs.add(int.class);
		clazzs.add(long.class);
		clazzs.add(float.class);
		clazzs.add(double.class);
		clazzs.add(char.class);
		clazzs.add(boolean.class);
		clazzs.add(Byte.class);
		clazzs.add(Short.class);
		clazzs.add(Integer.class);
		clazzs.add(Long.class);
		clazzs.add(Float.class);
		clazzs.add(Double.class);
		clazzs.add(Character.class);
		clazzs.add(Boolean.class);
	}

	@Override
	public void create(Object object) {
		if (null == object) {
			// 空值
		} else if (null != object.getClass() && object.getClass().isArray()) {
			// 数组
			baseDao.create(object);
		} else if (object instanceof java.util.List) {
			// 列表
			baseDao.create(object);
		} else if (object instanceof java.util.Set) {
			// 集合
			baseDao.create(object);
		} else {
			// JavaBean
			baseDao.create(object);
		}
	}

	@Override
	public List<?> retrieve(Object object) {
		return baseDao.retrieve(object);
	}

	@Override
	public List<?> retrieve(Where where) {
		return baseDao.retrieve(where);
	}

	@Override
	public List<?> retrieve(Where where, Page page) {
		return baseDao.retrieve(where, page);
	}

	@Override
	public PageResult retrieves(Where where, Page page) {
		return baseDao.retrieves(where, page);
	}

	@Override
	public void update(Object object) {
		if (null == object) {
			// 空值
		} else if (null != object.getClass() && object.getClass().isArray()) {
			// 数组
			baseDao.update(object);
		} else if (object instanceof java.util.List) {
			// 列表
			baseDao.update(object);
		} else if (object instanceof java.util.Set) {
			// 集合
			baseDao.update(object);
		} else {
			// JavaBean
			baseDao.update(object);
		}
	}

	@Override
	public void delete(Object object) {
		if (null == object) {
			// 空值
		} else if (null != object.getClass() && object.getClass().isArray()) {
			// 数组
			baseDao.delete(object);
		} else if (object instanceof java.util.List) {
			// 列表
			baseDao.delete(object);
		} else if (object instanceof java.util.Set) {
			// 集合
			baseDao.delete(object);
		} else {
			// JavaBean
			baseDao.delete(object);
		}
	}

	@Override
	public long count(Where where) {
		return baseDao.count(where);
	}

	public BaseDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao baseDao) {
		this.baseDao = baseDao;
	}

}

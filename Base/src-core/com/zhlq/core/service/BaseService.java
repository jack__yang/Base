package com.zhlq.core.service;

import java.util.List;

import com.zhlq.condition.Where;
import com.zhlq.page.Page;
import com.zhlq.page.PageResult;

/**
 * @ClassName BaseService
 * @Description 基础Service
 * @author ZHLQ
 * @date 2014年8月30日 上午1:38:48
 */
public interface BaseService {

	/**
	 * @Title create
	 * @Description 创建对象
	 * @适用条件：创建对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param object需要创建的对象
	 * @return void 返回类型
	 */
	void create(Object object);

	/**
	 * @Title retrieve
	 * @Description 查找对象
	 * @适用条件：查找对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param object需要查询的对象
	 * @return List<?> 返回类型
	 */
	List<?> retrieve(Object object);
	
	/**
	 * @Title retrieve
	 * @Description 查找对象
	 * @适用条件：查找对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param where查询条件
	 * @return List<?> 返回类型
	 */
	List<?> retrieve(Where where);
	
	/**
	 * @Title retrieve
	 * @Description 查找对象
	 * @适用条件：查找对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param where查询条件
	 * @param page分页对象
	 * @return List<?> 返回类型
	 */
	List<?> retrieve(Where where, Page page);
	
	/**
	 * @Title retrieve
	 * @Description 查找对象
	 * @适用条件：查找对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param where查询条件对象
	 * @param page分页对象
	 * @return PageResult<T> 返回类型
	 */
	PageResult retrieves(Where where, Page page);
	
	/**
	 * @Title update
	 * @Description 修改
	 * @适用条件：查找对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param object需要修改的对象
	 * @return void 返回类型
	 */
	void update(Object object);

	/**
	 * @Title delete
	 * @Description 删除对象
	 * @适用条件：创建对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param object需要删除的对象
	 * @return void 返回类型
	 */
	void delete(Object object);

	/**
	 * @Title count
	 * @Description 统计
	 * @param where查询对象
	 * @return long 返回类型
	 */
	long count(Where where);
}

package com.zhlq.core.dao.hbn;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.zhlq.condition.Sql;
import com.zhlq.condition.Where;
import com.zhlq.core.dao.BaseDao;
import com.zhlq.page.Page;
import com.zhlq.page.PageResult;

/**
 * @ClassName BaseDaoImpl
 * @Description 基础DaoImpl
 * @author ZHLQ
 * @date 2014年9月11日 下午9:01:57
 */
@Repository("baseDao")
public class BaseDaoImpl extends CoreDaoImpl implements BaseDao {

	/**
	 * @Fields clazzs : 基本数据类型和他们的封装类类型
	 */
	private static List<Class<?>> clazzs;
	
	static {
		clazzs = new ArrayList<Class<?>>();
		clazzs.add(byte.class);
		clazzs.add(short.class);
		clazzs.add(int.class);
		clazzs.add(long.class);
		clazzs.add(float.class);
		clazzs.add(double.class);
		clazzs.add(char.class);
		clazzs.add(boolean.class);
		clazzs.add(Byte.class);
		clazzs.add(Short.class);
		clazzs.add(Integer.class);
		clazzs.add(Long.class);
		clazzs.add(Float.class);
		clazzs.add(Double.class);
		clazzs.add(Character.class);
		clazzs.add(Boolean.class);
	}

	@Override
	public void create(Object object) {
		super.save(object);
	}

	@Override
	public void delete(Object object) {
		super.delete(object);
	}

	@Override
	public List<?> retrieve(Object object) {
//		Map<String, Object> map = ReflectUtils.getFieldAndValue(object);
		Where where = new Where(object.getClass());
//		where.setMode(Where.MODE_NOT_CHECK);
//		for (String key : map.keySet()) {
//			if (null != key && !"".equals(key) && null != map.get(key)
//					&& !"".equals(map.get(key))) {
//				where.addEqual(key, map.get(key));
//			}
//		}
		return this.retrieve(where);
	}

	@Override
	public List<?> retrieve(Where where) {
		return this.retrieve(where, null);
	}

	@Override
	public List<?> retrieve(Where where, Page page) {
		String hql = Sql.FROM + Sql.SPACE + where.getEntityName() + Sql.SPACE + Sql.WHERE_TRUE + Sql.SPACE + where.queryString();
		return super.getByHql(hql, where.getParams(), page);
	}

	@Override
	public PageResult retrieves(Where where, Page page) {
		PageResult pageResult = new PageResult();
		pageResult.setDatas(this.retrieve(where, page));
		pageResult.setPage(page);
		return pageResult;
	}

	@Override
	public void update(Object object) {
		super.update(object);
	}

	@Override
	public long count(Where where) {
		long result = 0;
		return result;
	}

	@Autowired
	@Override
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

}
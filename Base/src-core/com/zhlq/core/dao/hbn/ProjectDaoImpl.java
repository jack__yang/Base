package com.zhlq.core.dao.hbn;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.zhlq.core.dao.ProjectDao;

/**
 * @ClassName ProjectDaoImpl
 * @Description 项目DaoImpl
 * @author ZHLQ
 * @date 2014年9月6日 下午12:15:35
 */
@Repository("projectDao")
public class ProjectDaoImpl extends BaseDaoImpl implements ProjectDao {

	@Autowired
	@Override
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

}

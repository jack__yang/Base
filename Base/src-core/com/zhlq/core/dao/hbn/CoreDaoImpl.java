package com.zhlq.core.dao.hbn;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.zhlq.page.Page;

/**
 * @ClassName CoreDaoImpl
 * @Description 核心DaoImpl
 * @author ZHLQ
 * @date 2014年9月11日 下午9:01:57
 */
@Repository("coreDao")
public class CoreDaoImpl implements CoreDao {
	
	/**
	 * Session工厂类
	 */
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Serializable save(Object object) {
		return sessionFactory.openSession().save(object);
	}

	@Override
	public Serializable save(Object object, String entityName) {
		return sessionFactory.openSession().save(entityName, object);
	}

	@Override
	public void delete(Object object) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.delete(object);
		tx.commit();
	}

	@Override
	public void delete(Class<?> clazz, Serializable pk) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Object object = session.get(clazz, pk);
		session.delete(object);
		tx.commit();
	}

	@Override
	public void delete(Object object, String entityName) {
		sessionFactory.openSession().delete(entityName, object);
	}

	@Override
	public Object get(Class<?> clazz, Serializable id){
		return sessionFactory.openSession().get(clazz, id);
	}

	@Override
	public Object load(Class<?> clazz, Serializable id) {
		return sessionFactory.openSession().load(clazz, id);
	}

	@Override
	public void load(Object object, Serializable id) {
		sessionFactory.openSession().load(object, id);
	}

	@Override
	public Object load(String entityName, Serializable id) {
		return sessionFactory.openSession().load(entityName, id);
	}

	@Override
	public void update(Object object) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.update(object);
		tx.commit();
	}

	@Override
	public void update(Object object, String entityName) {
		sessionFactory.openSession().update(entityName, object);
	}

	@Override
	public void saveOrUpdate(Object object) {
		sessionFactory.openSession().saveOrUpdate(object);
	}

	@Override
	public void saveOrUpdate(Object object, String entityName) {
		sessionFactory.openSession().save(entityName, object);
	}

	@Override
	public List<?> getByHql(String hql, Object[] params) {
		return this.getByHql(hql, params, null);
	}

	@Override
	public List<?> getByHql(String hql, Object[] params, Page page) {
		Query query = sessionFactory.openSession().createQuery(hql);
		if(null != page){
			query.setFirstResult((int) (page.getSize() * page.getIndex()));
			query.setMaxResults((int) page.getSize());
		}
		if(null != params && params.length > 0){
			for(int i = 0; i < params.length; i++){
				query.setParameter(i, params[i]);
			}
		}
		return query.list();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}

package com.zhlq.core.dao.hbn;

import java.io.Serializable;
import java.util.List;

import com.zhlq.page.Page;

/**
 * @ClassName CoreDao
 * @Description 核心Dao接口
 * @author ZHLQ
 * @date 2015年3月5日 下午6:31:26
 */
public interface CoreDao {

	/**
	 * @Title save
	 * @Description 首先为给定的自由状态（Transient）的对象（根据配置）生成一个标识并赋值，然后将其持久化。
	 * @param object
	 * @return Serializable 返回类型
	 */
	Serializable save(Object object);

	/**
	 * @Title save
	 * @Description 首先为给定的自由状态（Transient）的对象（根据配置）生成一个标识并赋值，然后将其持久化。
	 * @param object
	 * @param entityName
	 * @return Serializable 返回类型
	 */
	Serializable save(Object object, String entityName);

	/**
	 * @Title delete
	 * @Description 从数据库中移除持久化（persistent）对象的实例。
	 * @param bean
	 * @return void 返回类型
	 */
	void delete(Object bean);

	/**
	 * @Title delete
	 * @Description 从数据库中移除持久化（persistent）对象的实例。
	 * @param bean
	 * @param entityName
	 * @return void 返回类型
	 */
	void delete(Object bean, String entityName);

	/**
	 * @Title delete
	 * @Description 从数据库中移除持久化（persistent）对象的实例。
	 * @param clazz
	 * @param pk
	 * @return void 返回类型
	 */
	void delete(Class<?> clazz, Serializable pk);

	/**
	 * @Title get
	 * @Description 根据给定标识和实体类返回持久化对象的实例，如果没有符合条件的持久化对象实例则返回null。
	 * @param clazz
	 * @param id
	 * @return Object持久化对象
	 */
	Object get(Class<?> clazz, Serializable id);

	/**
	 * @Title load
	 * @Description 将与给定的标示对应的持久化状态（值）复制到给定的自由状态（trasient）实例上。
	 * @param object
	 * @param id
	 * @return void 返回类型
	 */
	void load(Object object, Serializable id);

	/**
	 * @Title load
	 * @Description 在符合条件的实例存在的情况下，根据给定的实体类和标识返回持久化状态的实例。
	 * @param theClass
	 * @param id
	 * @return Object 返回类型
	 */
	Object load(Class<?> theClass, Serializable id);

	/**
	 * @Title load
	 * @Description 在符合条件的实例存在的情况下，根据给定的实体类和标识返回持久化状态的实例。
	 * @param entityName
	 * @param id
	 * @return Object 返回类型
	 */
	Object load(String entityName, Serializable id);

	/**
	 * @Title getByHql
	 * @Description 根据Hql获取实例列表
	 * @param hql
	 * @param params
	 * @return List<?> 返回类型
	 */
	List<?> getByHql(String hql, Object[] params);

	/**
	 * @Title getByHql
	 * @Description 根据Hql获取实例列表
	 * @param hql
	 * @param params
	 * @param page
	 * @return List<?> 返回类型
	 */
	List<?> getByHql(String hql, Object[] params, Page page);

	/**
	 * @Title update
	 * @Description 根据给定的detached（游离状态）对象实例的标识更新对应的持久化实例。
	 * @param object
	 * @return void 返回类型
	 */
	void update(Object object);

	/**
	 * @Title update
	 * @Description 根据给定的detached（游离状态）对象实例的标识更新对应的持久化实例。
	 * @param object
	 * @param entityName
	 * @return void 返回类型
	 */
	void update(Object object, String entityName);

	/**
	 * @Title saveOrUpdate
	 * @Description 根据给定的实例的标识属性的值（注：可以指定unsaved-value。一般默认null。）来决定执行 save() 或update()操作。
	 * @param object
	 * @return void 返回类型
	 */
	void saveOrUpdate(Object object);

	/**
	 * @Title saveOrUpdate
	 * @Description 根据给定的实例的标识属性的值（注：可以指定unsaved-value。一般默认null。）来决定执行 save() 或update()操作。
	 * @param entityName
	 * @param object
	 * @return void 返回类型
	 */
	void saveOrUpdate(Object object, String entityName);

}
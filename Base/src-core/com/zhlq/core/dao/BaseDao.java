package com.zhlq.core.dao;

import java.util.List;

import com.zhlq.condition.Where;
import com.zhlq.core.dao.hbntmpl.CoreDao;
import com.zhlq.page.Page;
import com.zhlq.page.PageResult;

/**
 * @ClassName BaseDao
 * @Description 基础Dao
 * @author ZHLQ
 * @date 2014年9月11日 下午8:55:44
 */
public interface BaseDao extends CoreDao {
	
	/**
	 * @Title create
	 * @Description 增
	 * @param object
	 * @return void 返回类型
	 */
	void create(Object object);

	/**
	 * @Title retrieve
	 * @Description 查
	 * @param object
	 * @return List<?> 返回类型
	 */
	List<?> retrieve(Object object);

	/**
	 * @Title retrieve
	 * @Description 查
	 * @param where条件
	 * @return List<T> 返回类型
	 */
	List<?> retrieve(Where where);

	/**
	 * @Title retrieve
	 * @Description 查
	 * @param where
	 * @param page
	 * @return List<?> 返回类型
	 */
	List<?> retrieve(Where where, Page page);
	
	/**
	 * @Title retrieves
	 * @Description 查
	 * @param where
	 * @param page
	 * @return PageResult 返回类型
	 */
	PageResult retrieves(Where where, Page page);

	/**
	 * @Title modify
	 * @Description 改
	 * @param object
	 * @return void 返回类型
	 */
	void update(Object object);

	/**
	 * @Title destroy
	 * @Description 删
	 * @param object
	 * @return void 返回类型
	 */
	void delete(Object object);
	
	/**
	 * @Title count
	 * @Description 统计
	 * @return long 返回类型
	 */
	long count();
	
	/**
	 * @Title count
	 * @Description 统计
	 * @param where
	 * @return long 返回类型
	 */
	long count(Where where);

}

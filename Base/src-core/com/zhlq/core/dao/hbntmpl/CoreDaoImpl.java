package com.zhlq.core.dao.hbntmpl;

import java.util.List;

import org.springframework.orm.hibernate3.HibernateTemplate;

import com.zhlq.condition.Where;
import com.zhlq.page.Page;

/**
 * @ClassName CoreDaoImpl
 * @Description 核心DaoImpl
 * @author ZHLQ
 * @date 2014年9月11日 下午9:01:57
 */
public class CoreDaoImpl implements CoreDao {
	
	/**
	 * @Fields hibernateTemplate : Hibernate模板
	 */
	private HibernateTemplate hibernateTemplate;

	@Override
	public void create(Object object) {
		hibernateTemplate.save(object);
	}

	@Override
	public List<?> retrieve(Object object) {
		String queryString = "";
		return hibernateTemplate.find(queryString);
	}

	public List<?> retrieve(Where where, Page page) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Object object) {
		hibernateTemplate.update(object);
	}

	@Override
	public void delete(Object object) {
		hibernateTemplate.delete(object);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long count(Where where) {
		// TODO Auto-generated method stub
		return 0;
	}

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

}

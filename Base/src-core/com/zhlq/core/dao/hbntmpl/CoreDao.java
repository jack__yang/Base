package com.zhlq.core.dao.hbntmpl;

import java.util.List;

import com.zhlq.condition.Where;

/**
 * @ClassName CoreDao
 * @Description 核心Dao接口
 * @author ZHLQ
 * @date 2015年3月15日 下午3:31:37
 */
public interface CoreDao {

	void create(Object object);

	List<?> retrieve(Object object);

	void update(Object object);

	void delete(Object object);

	long count();

	long count(Where where);

}
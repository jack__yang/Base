package com.zhlq.core.dao.hbntmpl;

import java.util.List;

import com.zhlq.condition.Where;
import com.zhlq.core.dao.BaseDao;
import com.zhlq.page.Page;
import com.zhlq.page.PageResult;

/**
 * @ClassName BaseDaoImpl
 * @Description 基础DaoImpl
 * @author ZHLQ
 * @date 2014年9月11日 下午9:01:57
 */
public class BaseDaoImpl extends CoreDaoImpl implements BaseDao {

	@Override
	public void create(Object object) {
		super.create(object);
	}

	@Override
	public List<?> retrieve(Object object) {
		return super.retrieve(object);
	}

	@Override
	public List<?> retrieve(Where where) {
		return this.retrieve(where, null);
	}

	@Override
	public List<?> retrieve(Where where, Page page) {
		return super.retrieve(where, page);
	}

	@Override
	public PageResult retrieves(Where where, Page page) {
		PageResult pageResult = new PageResult();
		pageResult.setDatas(this.retrieve(where, page));
		pageResult.setPage(page);
		return pageResult;
	}

	@Override
	public void update(Object object) {
		super.update(object);
	}

	@Override
	public void delete(Object object) {
		super.delete(object);
	}

	@Override
	public long count() {
		return super.count();
	}

	@Override
	public long count(Where where) {
		return super.count(where);
	}

}
package com.zhlq.core.constant;

/**
 * @ClassName CoreConstant
 * @Description 核心常量
 * @author ZHLQ
 * @date 2014年9月12日 上午12:16:12
 */
public class CoreConstant {

	/**
	 * @Fields PROP_FIELD_IS_EMPTY : 属性或者字段为空
	 */
	public static final String PROP_FIELD_IS_EMPTY = "属性或者字段为空！";

}

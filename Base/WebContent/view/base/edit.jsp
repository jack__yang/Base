<%@ page pageEncoding="UTF-8"%>
<html>
<head>
<%-- head begin --%>
<%@include file="/WEB-INF/common/headBegin.jsp" %>
<title>Base-ZHLQ</title>
<%-- head end --%>
<%@include file="/WEB-INF/common/headEnd.jsp" %>
</head>
<body>
	<%-- body begin --%>
	<%@include file="/WEB-INF/common/bodyBegin.jsp" %>	
	<%-- layout --%>
	<table class="ui-layout">
		<tbody>
			<tr>
				<td class="ui-layout-header" colspan="2">
					<%-- header --%>
					<%@include file="/WEB-INF/layout/header.jsp" %>
				</td>
			</tr>
			<tr>
				<td class="ui-layout-nav" colspan="2">
					<%-- navigation --%>
					<%@include file="/WEB-INF/component/nav.jsp" %>
				</td>
			</tr>
			<tr>
				<td class="ui-layout-menu">
					<div class="ui-layout-menu-div">
						<%-- menu --%>
						<%@include file="/WEB-INF/component/menu.jsp" %>
					</div>
					<%-- space --%>
					<div id="ui-layout-space"></div>
				</td>
				<td class="ui-layout-content">
					<%-- data --%>
					<%@include file="/WEB-INF/view/base/editData.jsp" %>
				</td>
			</tr>
			<tr>
				<td class="ui-layout-footer" colspan="2">
					<%-- footer --%>
					<%@include file="/WEB-INF/layout/footer.jsp" %>
				</td>
			</tr>
		</tbody>
	</table>
	<%-- body end --%>
	<%@include file="/WEB-INF/common/bodyEnd.jsp" %>
</body>
</html>
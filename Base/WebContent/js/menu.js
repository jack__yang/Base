$(document).ready(function() {
	// Store variables
	var menu_head = $('.ui-menu>li>a'),
		menu_body = $('.ui-menu li>.ui-menu-sub');

	// Open the first tab on load
	menu_head.first().addClass('ui-menu-active').next().slideDown('ui-menu-normal');

	// Click function
	menu_head.on('click', function(event) {
		// Disable header links
		event.preventDefault();

		// Show and hide the tabs on click
		if ($(this).attr('class') != 'ui-menu-active'){
			menu_body.slideUp('ui-menu-normal');
			$(this).next().stop(true,true).slideToggle('ui-menu-normal');
			menu_head.removeClass('ui-menu-active');
			$(this).addClass('ui-menu-active');
		}
	});
});
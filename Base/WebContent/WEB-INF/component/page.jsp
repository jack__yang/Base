<%@ page pageEncoding="UTF-8"%>
<link href="/Base/css/component/page.css" rel="stylesheet" type="text/css" />
<ul class="ui-page">
	<li><a title="首页" href="#">|&lt;</a></li>
	<li><a title="上一组" href="#">&lt;&lt;</a></li>
	<li><a title="上一页" href="#">&lt;</a></li>
	<li><a href="#">1</a></li>
	<li><a href="#">2</a></li>
	<li><a href="#">3</a></li>
	<li><a href="#">4</a></li>
	<li><a href="#">5</a></li>
	<li><a href="#">6</a></li>
	<li><a title="下一页" href="#">&gt;</a></li>
	<li><a title="下一组" href="#">&gt;&gt;</a></li>
	<li><a title="末页" href="#">&gt;|</a></li>
	<li>
		<%-- 不要换行 --%>
		<span>总537条&nbsp;每页</span><select>
			<option>10</option>
			<option>20</option>
			<option>50</option>
			<option>100</option>
			<option>200</option>
			<option>500</option>
			<option>1000</option>
		</select><span>条&nbsp;跳转到第</span><input title="输入页数按回车键确认" style="width:40px;"/><span>页</span>
	</li>
</ul>
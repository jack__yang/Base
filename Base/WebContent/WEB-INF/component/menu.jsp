<%@ page pageEncoding="UTF-8"%>
<link href="/Base/css/component/menu.css" rel="stylesheet" type="text/css" />
<ul class="ui-menu">
	<li class="ui-menu-mail">
		<a href="#two">权限管理<span>26</span></a>
		<ul class="ui-menu-sub">
			<li><a href="#"><em>01</em>用户管理<span>9</span></a></li>
			<li><a href="#"><em>02</em>角色管理<span>14</span></a></li>
			<li><a href="#"><em>03</em>资源管理<span>14</span></a></li>
			<li><a href="auth/restype/list.do"><em>04</em>资源类型<span>14</span></a></li>
		</ul>
	</li>
	<li class="ui-menu-files">
		<a href="#one">用户中心<span>495</span></a>
		<ul class="ui-menu-sub">
			<li><a href="#"><em>01</em>页面布局<span>42</span></a></li>
			<li><a href="#"><em>02</em>修改密码<span>87</span></a></li>
			<li><a href="#"><em>03</em>FTP Server<span>366</span></a></li>
			<li><a href="#"><em>04</em>Dropbox<span>1</span></a></li>
			<li><a href="#"><em>05</em>Skydrive<span>10</span></a></li>
		</ul>
	</li>
	<li class="ui-menu-files">
		<a href="#one">系统管理<span>495</span></a>
		<ul class="ui-menu-sub">
			<li><a href="#"><em>01</em>参数配置<span>42</span></a></li>
			<li><a href="#"><em>02</em>Skydrive<span>87</span></a></li>
			<li><a href="#"><em>03</em>FTP Server<span>366</span></a></li>
			<li><a href="#"><em>04</em>Dropbox<span>1</span></a></li>
			<li><a href="#"><em>05</em>Skydrive<span>10</span></a></li>
		</ul>
	</li>
	<li class="ui-menu-mail">
		<a href="#two">基础数据<span>26</span></a>
		<ul class="ui-menu-sub">
			<li><a href="#"><em>01</em>Hotmail<span>9</span></a></li>
			<li><a href="#"><em>02</em>Yahoo<span>14</span></a></li>
		</ul>
	</li>
	<li class="ui-menu-cloud">
		<a href="#three">Cloud<span>58</span></a>
		<ul class="ui-menu-sub">
			<li><a href="#"><em>01</em>Connect<span>12</span></a></li>
			<li><a href="#"><em>02</em>Profiles<span>19</span></a></li>
			<li><a href="#"><em>03</em>Options<span>27</span></a></li>
			<li><a href="#"><em>04</em>Connect<span>12</span></a></li>
			<li><a href="#"><em>05</em>Profiles<span>19</span></a></li>
			<li><a href="#"><em>06</em>Options<span>27</span></a></li>
		</ul>
	</li>
	<li class="ui-menu-sign">
		<a href="#four">Sign Out</a>
		<ul class="ui-menu-sub">
			<li><a href="#"><em>01</em>Log Out</a></li>
			<li><a href="#"><em>02</em>Delete Account</a></li>
			<li><a href="#"><em>03</em>Freeze Account<span>27</span></a></li>
		</ul>
	</li>
</ul>
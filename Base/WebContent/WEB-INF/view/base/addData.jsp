<%@ page pageEncoding="UTF-8"%>
<link href="/Base/css/list.css" rel="stylesheet" type="text/css"/>
<link href="/Base/css/add.css" rel="stylesheet" type="text/css"/>
<table class="ui-list">
	<thead>
		<tr>
			<th class="toTop ui-list-header-left">&nbsp;</th>
			<th class="toTop ui-list-header-center"><img src="/Base/img/operate/table.gif">新增XXX</th>
			<th class="toTop ui-list-header-right">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="ui-list-body-left"></td>
			<td class="ui-list-body-center">
				<form class="ui-list-query-form" action="">
					<table class="ui-add">
						<tbody>
							<tr>
								<td>表头1:</td>
								<td><input /></td>
								<td>表头2:</td>
								<td><input /></td>
							</tr>
							<tr>
								<td>表头3:</td>
								<td><input /></td>
								<td>表头4:</td>
								<td><input /></td>
							</tr>
							<tr>
								<td>21:</td>
								<td><input /></td>
								<td>22:</td>
								<td><input /></td>
							</tr>
							<tr>
								<td>23:</td>
								<td><input /></td>
								<td>24:</td>
								<td><input /></td>
							</tr>
							<tr>
								<td colspan="4">
									<input type="submit" value="提交"/>
									<input type="button" value="返回"/>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</td>
			<td class="ui-list-body-right"></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th class="toButtom ui-list-footer-left"></th>
			<th class="toButtom ui-list-footer-center">&nbsp;</th>
			<th class="toButtom ui-list-footer-right"></th>
		</tr>
	</tfoot>
</table>

<script>
	$(document).ready(function() { $(".ui-add").colorize(); });
</script>